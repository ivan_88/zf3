<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\ModuleManager\ModuleManager;
use Zend\I18n\Translator\Translator;
use Zend\Mvc\MvcEvent;

class Module
{

    private static $locales = ['en', 'ru', 'ua', 'pl'];

    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param ModuleManager $manager
     */
    public function init(ModuleManager $manager): void
    {
        $eventManager = $manager->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();
        $sharedEventManager->attach(__NAMESPACE__, 'dispatch', [$this, 'onDispatch'], 1);
    }

    /**
     * @param MvcEvent $event
     */
    public function onDispatch(MvcEvent $event): void
    {
        $lang = $event->getTarget()->params()->fromQuery('lang');
        $event->getApplication()
            ->getServiceManager()
            ->setService(
                'translator',
                Translator::factory(['locale' => in_array($lang, self::$locales) ? $lang : 'en'])
            );
    }
}
